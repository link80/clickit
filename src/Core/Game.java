package Core;

/**
 * Enthält allgemeine Informationen über das Spiel und das Spielfeld
 * 
 * @author Hannes Voss 551377
 *
 */
public class Game {
	public static int defaultGameFieldX = 15;		//Breite des Spielfelds
	public static int defaultGameFieldY = 15;		//Höhe des Spielfelds
	public static GameField g;						//Spielfeld-Objekt
	
	/**
	 * Der Konstruktor der Klasse Game (parameterlos)
	 */
	Game() {
		createGameField();
	}
	
	/**
	 * Hier wird das Spielfeld erstellt
	 */
	public void createGameField() {
		//This is where the magic happens . . .
		g			= new GameField(defaultGameFieldX, defaultGameFieldY, true, null);	//Erstellen des Spielfeld-Objekts
		
		//Print Int-Array des Spielfelds
//		int[][] fieldBytes = g.getFieldAsBytes();
//		
//		for(int tmpX = 1; tmpX <= GameField.getFieldSizeX(); tmpX++) {
//			for(int tmpY = 1; tmpY <= GameField.getFieldSizeY(); tmpY++) {
//				System.out.print("( " + fieldBytes[tmpX][tmpY] + " )");
//			}
//			System.out.println();
//		}
	}
}
