package Core;

import java.io.*;

/**
 * Klasse zum Auslesen von Konfigurationsdateien
 * 
 * @author Hannes Voss 551377
 *
 */
public class ConfigReader {
	private static int varOut[]; //Zu speichernde Werte
	
	/**
	 * Konstruktor der Klasse ConfigReader
	 * 
	 * @param count Gibt an, wie viele Werte aus der Config-Datei gelesen (bzw. gespeichert) werden sollen
	 */
	ConfigReader(int count) {
		varOut = new int[count];
		createConfig();
	}
	
	public void createConfig() {
		BufferedWriter f;
		try {
			f = new BufferedWriter(new FileWriter("config.ini"));
				f.write("### CONFIG FILE FOR CLICKIT ###");
				f.newLine();
				//f.write("[SCREEN]");
				f.newLine();
				//f.write("WIDTH=" + gameFieldX);
				f.newLine();
				//f.write("HEIGHT=" + gameFieldY);
				f.newLine();
				//f.write("COLORS=" + colors);
				f.newLine();
			f.close();
		} catch (IOException e) {
			System.out.println("Fehler beim Erstellen der Datei!");
		}
	}
	
	public static int[] getValuesAsArray() {
		return varOut;
	}
	
	public static int readInt(String lookFor) {
		return 0;
	}
}
