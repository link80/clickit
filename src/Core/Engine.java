package Core;

/**
 * Die Engine-Klasse des Spiels, sie stellt den Motor für alles dar und beinhaltet die wesentlichen Game-Objekte
 * 
 * @author Hannes Voss 551377
 *
 */
public class Engine {
	public static GUI w;
	public static Game g;
	public static ConfigReader c;
	
	/**
	 * Der Konstruktor der Engine-Klasse (parameterlos)
	 */
	Engine() {
		w = new GUI();				//GUI-Objekt wird erzeugt
		g = new Game();				//Das eigentliche Spiel-Objekt
		c = new ConfigReader(5);	//Fünf Werte sollen in der Config-Datei gespeichert werden!
	}
	
	void run() {
		System.out.println("~~~~~~~~~~~~~~");
		System.out.println("ClickIt - v0.9");
		System.out.println("~~~~~~~~~~~~~~");
	}
}
