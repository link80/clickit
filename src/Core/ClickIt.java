package Core;

/**
 * Die Hauptklasse des Spiels ClickIt (Mahki)
 * 
 * @version 0.9
 * @author Hannes Voss 551377
 */
public class ClickIt {
	public static void main(String[] args) {
		Engine e = new Engine();
		e.run();
	}
}