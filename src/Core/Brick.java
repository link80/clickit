package Core;

import java.awt.*;

import javax.swing.*;

/**
 * Die Klasse Brick, welche einen einzelnen Spielstein beschreibt und alle Informationen über diesen enthält
 * 
 * @author Hannes Voss 551377
 *
 */
public class Brick {
	static int count;
	
	public JButton label;
	public int posX;
	public int posY;
	private int type;
	
	/**
	 * Konstruktor der Klasse Brick
	 * 
	 * @param type Gibt den Farbtyp des Steins an (Rot, Blau, Grün, Gelb, Violett)
	 */
	Brick(int type) {	
		this.type = type;
		
		label = new JButton();
		label.setSize(GameField.brickSizeX, GameField.brickSizeY);
		label.setOpaque(true);
		
		if(this.type == 1) {
			label.setBackground(Color.RED);
		} else if (this.type == 2) {
			label.setBackground(Color.BLUE);
		} else if (this.type == 3) {
			label.setBackground(Color.GREEN);
		} else if (this.type == 4) {
			label.setBackground(Color.YELLOW);
		} else if (this.type == 5) {
			label.setBackground(new Color(255, 0, 255));
		} else if (this.type == -1) {
			this.label.setBackground(Color.BLACK);
		}
		
		GUI.frame.add(label);
		count++;
	}
	
	/**
	 * Getter-Methode für den Farbtyp des gewählten Spielsteins
	 * 
	 * @return Gibt den Farbtyp als Integer zurück
	 */
	public int getType() {
		return this.type;
	}
	
	/**
	 * Setzt den Farbtyp des gewählten Spielsteins
	 * 
	 * @param type Gibt den Farbtyp als Integer an
	 */
	public void setType(int type) {
		this.type = type;
		this.refresh();
	}
	
	/**
	 * Setzt die Position des gewählten Spielsteins
	 * 
	 * @param x Gibt die X-Koordinate des Steins an
	 * @param y Gibt die Y-Koordinate des Steins an
	 */
	public void setXY(int x, int y) {
		this.posX = x;
		this.posY = y;
		this.refresh();
	}
	
	/**
	 * Deaktiviert den gewählten Spielstein
	 */
	public void disableBrick() {
		this.label.setVisible(false);
		this.type = 0;
	}
	
	/**
	 * Aktualisiert die Farbe und Position des gewählten Spielsteins
	 */
	public void refresh() {
		this.label.setVisible(false);
		
		if(this.type == 1) {
			this.label.setBackground(Color.RED);
		} else if (this.type == 2) {
			this.label.setBackground(Color.BLUE);
		} else if (this.type == 3) {
			this.label.setBackground(Color.GREEN);
		} else if (this.type == 4) {
			this.label.setBackground(Color.YELLOW);
		} else if (this.type == 5) {
			this.label.setBackground(new Color(255, 0, 255));
		} else if (this.type == -1) {
			this.label.setBackground(Color.BLACK);
		}
		
		this.label.setLocation(this.posX, this.posY);
		this.label.setVisible(true);
	}
}
