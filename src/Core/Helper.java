package Core;

import java.util.Random;

/**
 * Die Helfer-Klasse enthält nützliche Hilfsmethoden
 * 
 * @author Hannes Voss 551377
 *
 */
public class Helper {
	public static final int RED		= 1;
	public static final int BLUE	= 2;
	public static final int GREEN	= 3;
	public static final int YELLOW	= 4;
	public static final int VIOLET	= 5;
	
	public static int getRandom(int[][] array) {
		int[] newArray = new int[array.length];
		Array.fill(newArray, 0);
		int j = 0;
		for (int i = 0; i < array.length; i++) {
			if(array[i][1] == 1) {
				newArray[j] = array[i][0];
				j++;
			}
		}
		for(int t = 0; t < array.length; t++) {
			if(newArray[t] == 0) {
				newArray = Array.CutArray(newArray, t);
				break;
			}
		}
		return getRandom(newArray);
	}
	
	public static int getRandom(int[] array) {
		int rnd = new Random().nextInt(array.length);
		return array[rnd];
	}
}