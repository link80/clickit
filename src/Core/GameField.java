package Core;

import java.awt.event.*;
import javax.swing.*;

/**
 * Die Klasse GameField enthält alle Informationen über das eigentliche Spiel.
 * Sie enthält außerdem alle nötigen Methoden zur Spielsteuerung.
 * 
 * @author Hannes Voss 551377
 *
 */
public class GameField {
	private static String	playerName;
	private static int		playerScore;
	public static String[][] highscore;
	private static int highscoreCount;
	
	public static int[][]	colors = {
		//0 -> Rot; 1 -> Blau; 2 -> Grün; 3 -> Gelb; 4 -> Violett
		{0, 0}, {Helper.RED, 1}, {Helper.BLUE, 1}, {Helper.GREEN, 1}, {Helper.YELLOW, 1}, {Helper.VIOLET, 1}
	};
	private static int		fieldSizeX;
	private static int		fieldSizeY;
	public static int		brickSizeX = 20;
	public static int		brickSizeY = 20;
	public static Brick[][]	bricks;
	private static int[][]	values;
	private static int[][]	valuesStart;	//Der Ursprungszustand des Feldes
	private static int[][]	lastState;	//Letzter Stand des Spielfelds
	private static int		zuege;
	
	/**
	 * Der Konstruktor der Klasse GameField
	 * 
	 * @param sizeX Gibt die Spielfeldbreite an
	 * @param sizeY Gibt die Spielfeldhöhe an
	 */
	GameField(int sizeX, int sizeY, boolean random, int[][] startingValues) {
		fieldSizeX	= sizeX;		//Setze die Anzahl der Spalten des Spielfelds
		fieldSizeY	= sizeY;		//Setze die Anzahl der Zeilen des Spielfelds
		
		zuege		= 0;			//Initialisiere die Anzahl der gemachten Züge mit 0
		playerScore	= 0;			//Initialisiere die Punkte des Spiels mit 0
		highscore	= new String[10][2];
		
		bricks		= new Brick[fieldSizeX + 2][fieldSizeY + 2];	//Array der eigentlichen Spielsteine
		values		= new int[fieldSizeX + 2][fieldSizeY + 2];		//Array mit den Farbwerten der Spielsteine
		valuesStart	= new int[fieldSizeX + 2][fieldSizeY + 2];		//Array mit den Ausgangswerten eines neuen Spiels
		lastState	= new int[fieldSizeX + 2][fieldSizeY + 2];		//Array mit dem zuletzt gespeicherten Spielstand (Undo-Feld)
		
		for(int i = 0; i < (fieldSizeX + 2); i++){
			for(int j = 0; j < (fieldSizeY + 2); j++){
				bricks[i][j] = new Brick(-1);
				values[i][j] = -1;
				valuesStart[i][j] = -1;
			}
		}
		
		int setX = 0;
		int setY = 0;
		int tmpType;
		
		ButtonActionListener actionListener = new ButtonActionListener();
		
		//Spielfeld erstellen
		for(int tmpX = 1; tmpX <= fieldSizeX; tmpX++) {
			for(int tmpY = 1; tmpY <= fieldSizeY; tmpY++) {
				if(random == true){
					tmpType = Helper.getRandom(colors);
					
					values[tmpX][tmpY] = tmpType;			//Die aktuelle Spiel-Matrix füllen
					valuesStart[tmpX][tmpY] = tmpType;		//Den Ursprungszustand des Feldes zwischenspeichern!
					lastState[tmpX][tmpY] = tmpType;
					
					bricks[tmpX][tmpY].setType(tmpType);	//Erster Parameter gibt den Farb-Typ des Steins an!
				} else {
					values[tmpX][tmpY] = startingValues[tmpX][tmpY];
					valuesStart[tmpX][tmpY] = startingValues[tmpX][tmpY];	//Den Ursprungszustand des Feldes zwischenspeichern!
					lastState[tmpX][tmpY] = startingValues[tmpX][tmpY];
					bricks[tmpX][tmpY].setType(startingValues[tmpX][tmpY]);	//Erster Parameter gibt den Farb-Typ des Steins an!
				}
					
				bricks[tmpX][tmpY].setXY(setX, setY);
				bricks[tmpX][tmpY].label.addActionListener(actionListener);
					
				setX = setX + brickSizeX;
			}
			setX = 0;
			setY = setY + brickSizeY;
		}
		
//		lastState = valuesStart;
		
		refreshBricks();
		GUI.frame.setSize(fieldSizeY * brickSizeY, (fieldSizeX * brickSizeX) + brickSizeX);
	}
	
	/**
	 * Getter-Methode, um das Spielfeld als Array darzustellen
	 * @return Gibt das Spielfeld als zweidimensionales Integerarray zurück
	 */
	public int[][] getFieldAsBytes() {
		return values;
	}
	
	public int[][] getStartingFieldAsIntArr() {
		return valuesStart;
	}
	
	public static void zug(int zn, int sn) {
		int clickedField;
		if ((zn < 1) | (zn > fieldSizeX) | (sn < 1) | (sn > fieldSizeY)) {
			System.out.println("Feldposition ist falsch - neuer Zug!");
		} else {
			clickedField = values[zn][sn];
			//Test ob zulaessige Feldposition angeklickt wurde
			if (values[zn][sn] == -1) {	
				JOptionPane.showMessageDialog(null, "Feld ist leer - neuer Zug!", "Nicht möglich", JOptionPane.WARNING_MESSAGE);
			} else if ((values[zn][sn - 1] != clickedField) && (values[zn][sn + 1] != clickedField)
					&& (values[zn - 1][sn] != clickedField) && (values[zn + 1][sn] != clickedField)) {
				System.out.println("Kein mehrfaches Feld - neuer Zug!");
			} else {
				//UNDO absichern
				for(int x = 1; x <= GameField.getFieldSizeX(); x++) {
					for(int y = 1; y <= GameField.getFieldSizeY(); y++) {
						lastState[x][y] = values[x][y];
					}
				}
				
				values[zn][sn] = -1;				//Angeklicktes Feld löschen
				loeschen(zn, sn, clickedField);		//Umliegende Felder löschen
				spaltenrunter();					//Eventuelle Felder von oben nachrutschen lassen
				
				if(isColumnEmpty()) {
					spaltenNachRechts();
					playerScore = playerScore + 10;
				}
				
				GameField.refreshBricks();
				zuege++;
				
				if(isFieldEmpty()) {
					highscore[highscoreCount][0] = Integer.toString(playerScore);
					highscore[highscoreCount][1] = playerName;
					highscoreCount++;
					JOptionPane.showMessageDialog(null, "Glückwunsch, " + playerName + "! Sie haben mit " + zuege +  " Zügen gewonnen! (Punkte: " + playerScore + ")", "Yay!", JOptionPane.INFORMATION_MESSAGE);
				} else if(zugMoeglich() == false) {
					JOptionPane.showMessageDialog(null, "Sie haben verloren!", "Oh nein ...", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
	}
	
	public static void loeschen(int zn, int sn, int value) {
			playerScore = playerScore + 1;
			if (values[zn - 1][sn] == value){
				values[zn - 1][sn] = -1;
				loeschen(zn-1, sn, value);
			}
			if (values[zn][sn - 1] == value){
				values[zn][sn - 1] = -1;
				loeschen(zn, sn - 1, value);
			}
			if (values[zn + 1][sn] == value){
				values[zn + 1][sn] = -1;
				loeschen(zn + 1, sn, value);
			}
			if (values[zn][sn + 1] == value){
				values[zn][sn + 1] = -1;
				loeschen(zn, sn + 1, value);
			}
	}
	
	public static void spaltenrunter() {
		for (int j = 1; j <= fieldSizeY; j++) {
			for (int i = 1; i <= fieldSizeY; i++) {
				if (values[i][j] == -1) {
					for (int k = i; k > 0; k--) {
						values[k][j] = values[k - 1][j];
					}
					values[1][j] = -1;
				}
			}
		}
	}
	
	public static void spaltenNachRechts() {
		for(int c = 0; c <= fieldSizeX; c++) {
			for(int i = fieldSizeX; i >= 1; i--) {	
				if(values[fieldSizeY][i] == -1) {
					for(int x = fieldSizeY; x >= 1; x--) {
						values[x][i] = values[x][i - 1];
						values[x][i - 1] = -1;
					}
				}
			}
		}
	}
	
	public static boolean isColumnEmpty() {
		boolean isFree = false;
		for(int c = 0; c <= fieldSizeX; c++) {
			for(int i = fieldSizeX; i >= 1; i--) {	
				if(values[fieldSizeY][i] == -1) {
					isFree = true;
				}
			}
		}
		return isFree;
	}
	
	public static boolean isFieldEmpty() {
		boolean isFree = true;
		for(int x = 1; x <= fieldSizeX; x++) {
			for(int y = 1; y <= fieldSizeY; y++) {
				if(values[x][y] != -1) {
					isFree = false;
				}
			}
		}
		return isFree;
	}
	
	public static boolean zugMoeglich() {
		int countPossibleActions = 0;	//Gibt die Anzahl der möglichen Züge an
		//Feld durchgehen und für jeden Stein prüfen ob Nachbarsteine vorhanden
		for(int x = 1; x <= fieldSizeX; x++) {
			for(int y = 1; y <= fieldSizeY; y++) {
				if(values[x][y] != -1) {
					if((values[x][y - 1] != values[x][y]) && (values[x][y + 1] != values[x][y]) && (values[x - 1][y] != values[x][y]) && (values[x + 1][y] != values[x][y])) {
					} else {
						//Wenn ein Feld noch Nachbarn hat, dann..
						countPossibleActions++;
					}
				}
			}
		}
		if(countPossibleActions != 0) {		//Wenn noch Züge möglich sind
			return true;
		} else {							//Wenn keine Züge mehr möglich sind
			return false;
		}
	}
	
	public static void setPlayerName(String tmpName) {
		playerName = tmpName;
	}
	
	public static void setGameField(int[][] array) {
		for(int x = 1; x < GameField.getFieldSizeX(); x++) {
			for(int y = 1; y < GameField.getFieldSizeX(); y++) {
				values[x][y] = array[x][y];
				//valuesStart[x][y] = array[x][y];			//Den Ursprungszustand des Feldes zwischenspeichern!
				bricks[x][y].setType(array[x][y]);			//Erster Parameter gibt den Farb-Typ des Steins an!
			}
		}
		GameField.refreshBricks();
	}
	
	public static void refreshBricks() {
		for(int x = 1; x <= GameField.fieldSizeX; x++) {
			for(int y = 1; y <= GameField.fieldSizeY; y++) {
				bricks[x][y].setType(values[x][y]);
				bricks[x][y].refresh();
			}
		}
		GUI.frame.repaint();
	}
	
	public void clearAll() {
		for(int i = 1; i <= fieldSizeX; i++) {
			for(int j = 1; j <= fieldSizeY; j++) {
				GUI.frame.remove(bricks[i][j].label);
			}
		}
	}
	
	public static int[][] getLastState() {
		return lastState;
	}
	
	public static void gitterNetz(boolean state) {
		for(int i = 1; i <= fieldSizeX; i++) {
			for(int j = 1; j <= fieldSizeY; j++) {
				bricks[i][j].label.setBorderPainted(state);
			}
		}
		GUI.frame.repaint();
	}
	
	public static int getFieldSizeX() {
		return fieldSizeX;
	}
	
	public static int getFieldSizeY() {
		return fieldSizeY;
	}
}

/**
 * Klasse um Events der Buttons abzufangen
 * 
 * @author Hannes Voss 551377
 *
 */
class ButtonActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
			JButton a = (JButton)e.getSource();
			
			//Warum auch immer --> Geht nur QUADRATISCH ! ! !
			for(int x = 1; x <= GameField.getFieldSizeX(); x++) {
				for(int y = 1; y <= GameField.getFieldSizeY(); y++) {
					if(a == GameField.bricks[x][y].label) {
						GameField.zug(x, y);
					}
				}
			}
		}
	}
}
