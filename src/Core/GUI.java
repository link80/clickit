package Core;

import java.awt.event.*;
import javax.swing.*;

/**
 * Die GUI-Klasse, welche das aktuelle Fenster und alle Schaltflächen beinhaltet
 * 
 * @author Hannes Voss 551377
 *
 */
public class GUI {
	public static JFrame frame;
	public static JDialog highscoreFrame;
	public static boolean gitterNetz = true;
	private String rules = 
			"Das Ziel ist es, alle Steine abzuräumen.\nDazu muss ein Stein mit der Maus angetippt werden, der neben mindestens einem gleichfarbigen Stein liegt.\nWurde eine Spalte abgeräumt, rücken die anderen Spalten nach rechts.\nBeendet ist das Spiel, wenn alle Steine weg sind oder sich keine mehr abräumen lassen.";
	private String credits = 
			"Programmiert und designt von Hannes Voss\nAlle Symbole von Hannes Voss";
	
	/**
	 * Konstruktor der Klasse GUI
	 */
	GUI() {
		createGUI();
	}
	
	/**
	 * Erstellt die GUI mit allen Elementen
	 */
	public void createGUI() {
		//Nach Namen fragen
		GameField.setPlayerName(JOptionPane.showInputDialog(null, "Geben Sie Ihren Namen ein: ", "Name ..."));
		
		//Fenster erstellen
		frame = new JFrame("ClickIt");
		
		ImageIcon frameIcon = new ImageIcon("img/frameIcon.png");
		
		frame.setIconImage(frameIcon.getImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Layout festlegen
		frame.setLayout(null);
		
		//Icons laden
		ImageIcon gameStartIcon		= new ImageIcon("img/gameStart.png");
		ImageIcon gameUndoIcon		= new ImageIcon("img/gameUndo.png");
		ImageIcon gameLoadIcon		= new ImageIcon("img/gameLoad.png");
		ImageIcon gameRandomIcon	= new ImageIcon("img/gameRandom.png");
		ImageIcon gameSizeIcon		= new ImageIcon("img/gameSize.png");
		ImageIcon gameBgIcon		= new ImageIcon("img/gameBg.png");
		//ImageIcon gameNetIcon		= new ImageIcon("img/gameNet.png");
		ImageIcon gameSaveIcon		= new ImageIcon("img/gameSave.png");
		ImageIcon gameRulesIcon		= new ImageIcon("img/gameRules.png");
		ImageIcon highscoreIcon		= new ImageIcon("img/highscore.png");
		
		final ImageIcon colorRedIcon		= new ImageIcon("img/color_red.png");
		final ImageIcon colorBlueIcon		= new ImageIcon("img/color_blue.png");
		final ImageIcon colorGreenIcon		= new ImageIcon("img/color_green.png");
		final ImageIcon colorYellowIcon		= new ImageIcon("img/color_yellow.png");
		final ImageIcon colorVioletIcon		= new ImageIcon("img/color_violet.png");
		
		final ImageIcon colorRedOnIcon		= new ImageIcon("img/color_red_on.png");
		final ImageIcon colorBlueOnIcon		= new ImageIcon("img/color_blue_on.png");
		final ImageIcon colorGreenOnIcon	= new ImageIcon("img/color_green_on.png");
		final ImageIcon colorYellowOnIcon	= new ImageIcon("img/color_yellow_on.png");
		final ImageIcon colorVioletOnIcon	= new ImageIcon("img/color_violet_on.png");
		
		final ImageIcon on				= new ImageIcon("img/on.png");
		final ImageIcon off				= new ImageIcon("img/off.png");
		
		//Menüleiste erstellen
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		//Menüregister einbinden
		JMenu gameMenu = new JMenu("Spiel");
		menuBar.add(gameMenu);
		
		JMenu colorMenu = new JMenu("Farben");
		menuBar.add(colorMenu);
		
		JMenu settingsMenu = new JMenu("Einstellungen");
		menuBar.add(settingsMenu);
		
		JMenu helpMenu = new JMenu("Hilfe");
		menuBar.add(helpMenu);
		
		//Menüpunkte einbinden
		//--> "SPIEL"
		JMenuItem newgameItem = new JMenuItem("Neu Starten", gameStartIcon);
		newgameItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[][] altesSpielfeld = Game.g.getStartingFieldAsIntArr();
				Game.g.clearAll();
				int x = GameField.getFieldSizeX();
				int y = GameField.getFieldSizeY();
				//GameField.setPlayerName(JOptionPane.showInputDialog(null, "Geben Sie Ihren Namen ein: ", "Name ..."));
				Game.g = new GameField(x, y, false, altesSpielfeld);
			}
		});
		gameMenu.add(newgameItem);
		
		JMenuItem undoItem = new JMenuItem("Rückgängig machen", gameUndoIcon);
		undoItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//UNDO realisieren
					int[][] lastAction = GameField.getLastState();
					GameField.setGameField(lastAction);
				}
		});
		gameMenu.add(undoItem);
		
		gameMenu.addSeparator();
		
		JMenuItem choosegameItem = new JMenuItem("Spiel Auswählen ...", gameLoadIcon);
		choosegameItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser openGame = new JFileChooser("Spiel laden ...");
				openGame.showOpenDialog(null);
			}
		});
		gameMenu.add(choosegameItem);
		
		JMenuItem startrandomItem = new JMenuItem("Zufälliges Spiel Spielen", gameRandomIcon);
		startrandomItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Game.g.clearAll();
				int x = GameField.getFieldSizeX();
				int y = GameField.getFieldSizeY();
				GameField.setPlayerName(JOptionPane.showInputDialog(null, "Geben Sie Ihren Namen ein: ", "Name ..."));
				Game.g = new GameField(x, y, true, null);
			}
		});
		gameMenu.add(startrandomItem);
		
		gameMenu.addSeparator();
		
		JMenuItem highscoreItem = new JMenuItem("Highscore", highscoreIcon);
		highscoreItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Highscore-Fenster erstellen
				highscoreFrame = new JDialog();
				highscoreFrame.setTitle("Highscore");
				highscoreFrame.setSize(120,250);
				highscoreFrame.setLayout(null);
				highscoreFrame.setResizable(false);
				int y = 0;
				JLabel[] content = new JLabel[10];
				for(int i = 0; i < GameField.highscore.length; i++) {
					content[i] = new JLabel((i+1) + ". " + GameField.highscore[i][0] + " " + GameField.highscore[i][1], null, JLabel.CENTER);
					content[i].setLocation(0, y);
					content[i].setSize(content[i].getPreferredSize());
					highscoreFrame.add(content[i]);
					y = y + 20;
				}
				highscoreFrame.setVisible(true);
			}
		});
		gameMenu.add(highscoreItem);
		
		gameMenu.addSeparator();
		
		JMenuItem exitItem = new JMenuItem("Beenden");
		exitItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
				}
		});
		gameMenu.add(exitItem);
		
		//--> "FARBEN"
		final JMenuItem redItem = new JMenuItem("Rot", colorRedOnIcon);
		redItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(GameField.colors[Helper.RED][1] == 1) {
					//Farbe entfernen
					redItem.setIcon(colorRedIcon);
					GameField.colors[Helper.RED][1] = 0;
				} else if (GameField.colors[Helper.RED][1] == 0) {
					//Farbe hinzufügen
					redItem.setIcon(colorRedOnIcon);
					GameField.colors[Helper.RED][1] = 1;
				}
				System.out.println("RED -> " + GameField.colors[Helper.RED][1]);
			}
		});
		colorMenu.add(redItem);
		
		final JMenuItem blueItem = new JMenuItem("Blau", colorBlueOnIcon);
		blueItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(GameField.colors[Helper.BLUE][1] == 1) {
					//Farbe entfernen
					blueItem.setIcon(colorBlueIcon);
					GameField.colors[Helper.BLUE][1] = 0;
				} else if (GameField.colors[Helper.BLUE][1] == 0) {
					//Farbe hinzufügen
					blueItem.setIcon(colorBlueOnIcon);
					GameField.colors[Helper.BLUE][1] = 1;
				}
				System.out.println("BLUE -> " + GameField.colors[Helper.BLUE][1]);
			}
		});
		colorMenu.add(blueItem);
		
		final JMenuItem greenItem = new JMenuItem("Grün", colorGreenOnIcon);
		greenItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(GameField.colors[Helper.GREEN][1] == 1) {
					//Farbe entfernen
					greenItem.setIcon(colorGreenIcon);
					GameField.colors[Helper.GREEN][1] = 0;
				} else if (GameField.colors[Helper.GREEN][1] == 0) {
					//Farbe hinzufügen
					greenItem.setIcon(colorGreenOnIcon);
					GameField.colors[Helper.GREEN][1] = 1;
				}
				System.out.println("GREEN -> " + GameField.colors[Helper.GREEN][1]);
			}
		});
		colorMenu.add(greenItem);
		
		final JMenuItem yellowItem = new JMenuItem("Gelb", colorYellowOnIcon);
		yellowItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(GameField.colors[Helper.YELLOW][1] == 1) {
					//Farbe entfernen
					yellowItem.setIcon(colorYellowIcon);
					GameField.colors[Helper.YELLOW][1] = 0;
				} else if (GameField.colors[Helper.YELLOW][1] == 0) {
					//Farbe hinzufügen
					yellowItem.setIcon(colorYellowOnIcon);
					GameField.colors[Helper.YELLOW][1] = 1;
				}
				System.out.println("YELLOW -> " + GameField.colors[Helper.YELLOW][1]);
			}
		});
		colorMenu.add(yellowItem);
		
		final JMenuItem purpleItem = new JMenuItem("Violett", colorVioletOnIcon);
		purpleItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(GameField.colors[Helper.VIOLET][1] == 1) {
					//Farbe entfernen
					purpleItem.setIcon(colorVioletIcon);
					GameField.colors[Helper.VIOLET][1] = 0;
				} else if (GameField.colors[Helper.VIOLET][1] == 0) {
					//Farbe hinzufügen
					purpleItem.setIcon(colorVioletOnIcon);
					GameField.colors[Helper.VIOLET][1] = 1;
				}
				System.out.println("VIOLET -> " + GameField.colors[Helper.VIOLET][1]);
			}
		});
		colorMenu.add(purpleItem);
		
		//--> "EINSTELLUNGEN"
		JMenuItem sizeItem = new JMenuItem("Spalten & Zeilen ...", gameSizeIcon);
		sizeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Fenster für Eingabe von Spielfeldgröße öffnen
				String newGameSize = JOptionPane.showInputDialog(null, "Neue Größe: ", "10 bis 40");
				int nGameSize = Integer.parseInt(newGameSize);
				if (nGameSize < 10 || nGameSize > 40) {
					JOptionPane.showMessageDialog(null, "Geben Sie eine korrekte Spielfeldgröße ein!", "Nicht möglich", JOptionPane.ERROR_MESSAGE);
				} else {
					Game.g.clearAll();
					GameField.setPlayerName(JOptionPane.showInputDialog(null, "Geben Sie Ihren Namen ein: ", "Name ..."));
					Game.g = new GameField(nGameSize, nGameSize, true, null);
				}
			}
		});
		settingsMenu.add(sizeItem);
		
		JMenuItem bgItem = new JMenuItem("Hintergrundbild ...", gameBgIcon);
		bgItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser openBG = new JFileChooser("Hintergrundbild laden ...");
				openBG.showOpenDialog(null);
			}
		});
		settingsMenu.add(bgItem);
		
		final JMenuItem netItem = new JMenuItem("Gitternetz Anzeigen", on);
		netItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//boolean ob gitternetz an oder aus ist
				if(gitterNetz == true) {
					netItem.setIcon(off);
					GameField.gitterNetz(false);
					gitterNetz = false;
				} else if (gitterNetz == false) {
					netItem.setIcon(on);
					GameField.gitterNetz(true);
					gitterNetz = true;
				}
			}
		});
		settingsMenu.add(netItem);
		
		settingsMenu.addSeparator();
		
		JMenuItem saveItem = new JMenuItem("Speichern", gameSaveIcon);
		saveItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser saveGame = new JFileChooser("Spiel speichern ...");
				saveGame.showSaveDialog(null);
			}
		});
		settingsMenu.add(saveItem);
		
		JMenuItem restoreItem = new JMenuItem("Wiederherstellen");
		settingsMenu.add(restoreItem);
		
		//--> "HILFE"
		JMenuItem rulesItem = new JMenuItem("Spielregeln", gameRulesIcon);
		rulesItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, rules, "Spielregeln", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		helpMenu.add(rulesItem);
		
		helpMenu.addSeparator();
		
		JMenuItem aboutItem = new JMenuItem("Über dieses Spiel ...");
		helpMenu.add(aboutItem);
		
		JMenuItem creditsItem = new JMenuItem("Credits");
		creditsItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, credits, "Credits", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		helpMenu.add(creditsItem);
		
		//Toolbar Buttons definieren
		JButton gameStartButton = new JButton();
		gameStartButton.setIcon(gameStartIcon);
		
		JButton gameUndoButton = new JButton();
		gameUndoButton.setIcon(gameUndoIcon);
		
		JButton gameLoadButton = new JButton();
		gameLoadButton.setIcon(gameLoadIcon);
		
		JButton gameRandomButton = new JButton();
		gameRandomButton.setIcon(gameRandomIcon);
		
		JButton gameBgButton = new JButton();
		gameBgButton.setIcon(gameBgIcon);
		
		JButton gameSizeButton = new JButton();
		gameSizeButton.setIcon(gameSizeIcon);
		
		JButton gameSaveButton = new JButton();
		gameSaveButton.setIcon(gameSaveIcon);
		
		JButton gameRulesButton = new JButton();
		gameRulesButton.setIcon(gameRulesIcon);
		
		//Toolbar definieren
		JToolBar toolbar = new JToolBar("Toolbar");
		
		toolbar.add(gameStartButton);
		toolbar.add(gameUndoButton);
		toolbar.addSeparator();
		toolbar.add(gameLoadButton);
		toolbar.add(gameRandomButton);
		toolbar.addSeparator();
		toolbar.add(gameBgButton);
		toolbar.add(gameSizeButton);
		toolbar.addSeparator();
		toolbar.add(gameSaveButton);
		toolbar.addSeparator();
		toolbar.add(gameRulesButton);
		
		toolbar.setFloatable(false);
		
		//Setzen der Fenstereigenschaften
		frame.setResizable(false);
		frame.setVisible(true);
	}
}