package Core;

/**
 * Hilfsklasse zum Arbeiten mit Arrays
 * 
 * @author Hannes Voss 551377
 *
 */
public class Array {
	public static double mittelwertArr(double[] tmp) {
		double gl = 0;				//Initialisieren der temporär verwendeten Variable
		for (double elem : tmp) {	//Gehe alle Elemente des Arrays durch
			gl = gl + elem;			//Zähle die Inhalte der Array-Felder zusammen
		}
		gl = gl / tmp.length;		//Teile die Summe aller Array-Felder durch die Array-Länge
		return gl;					//Gebe das Ergebnis (den Mittelwert) zurück
	}
	public static int[] minMaxArr(int[] tmp) {
		int[] minMaxArr = new int[2];		//Initialisieren eines temporären Arrays
		minMaxArr[0] = tmp[0];				//Setze erstes Feld auf das erste übergebene Feld (da dies aktuell das Minimum ist)
		minMaxArr[1] = tmp[0];				//Setze zweites Feld auf das erste übergebene Feld (da dies auch aktuell das Maximum ist)
		for (int elem : tmp) {				//Gehe jedes Feld durch (für das Minimum)
			if (minMaxArr[0] > elem) {		//Wenn das aktuell gespeicherte Minimum größer ist als das Feld des übergebenen Arrays...
				minMaxArr[0] = elem;		//...dann ersetze das Minimum mit dem tatsächlichen aktuellen Minimum
			}
		}
		for (int elem : tmp) {				//Gehe jedes Feld durch (für das Maximum)
			if (minMaxArr[1] < elem) {		//Wenn das aktuell gespeicherte Maximum kleiner ist als das Feld des übergebenen Arrays...
				minMaxArr[1] = elem;		//...dann ersetze das Maximum mit dem tatsächlichen aktuellen Maximum
			}
		}
		return minMaxArr;					//Gib das Array zurück ([0] -> Minimum ; [1] -> Maximum)
	}
	public static int[] minMaxArr(String[][] tmp) {
		int[] minMaxArr = new int[2];		//Initialisieren eines temporären Arrays
		minMaxArr[0] = Integer.getInteger(tmp[0][0]);				//Setze erstes Feld auf das erste übergebene Feld (da dies aktuell das Minimum ist)
		minMaxArr[1] = Integer.getInteger(tmp[0][0]);				//Setze zweites Feld auf das erste übergebene Feld (da dies auch aktuell das Maximum ist)
		for (int i = 0; i < tmp.length; i++) {				//Gehe jedes Feld durch (für das Minimum)
			if (minMaxArr[0] > Integer.getInteger(tmp[i][0])) {		//Wenn das aktuell gespeicherte Minimum größer ist als das Feld des übergebenen Arrays...
				minMaxArr[0] = Integer.getInteger(tmp[i][0]);		//...dann ersetze das Minimum mit dem tatsächlichen aktuellen Minimum
			}
		}
		for (int j = 0; j < tmp.length; j++) {				//Gehe jedes Feld durch (für das Maximum)
			if (minMaxArr[1] < Integer.getInteger(tmp[j][0])) {		//Wenn das aktuell gespeicherte Maximum kleiner ist als das Feld des übergebenen Arrays...
				minMaxArr[1] = Integer.getInteger(tmp[j][0]);		//...dann ersetze das Maximum mit dem tatsächlichen aktuellen Maximum
			}
		}
		return minMaxArr;					//Gib das Array zurück ([0] -> Minimum ; [1] -> Maximum)
	}
	public static int[] CutArray(int[] temp, int length) {
		int[] newArray = new int[length];				//Initialisiere ein temporäres Array mit der übergebenen Länge
		for (int i = 0; i < newArray.length; i++) {		//Gehe solange, wie das neue Array lang ist/sein soll
			newArray[i] = temp[i];						//Kopiere alle Felder des übergebenen Arrays zu dem neuen Array
		}
		return newArray;								//Gebe das neue (gekürzte) Array zurück
	}
	public static void print2DArray(int[][] temp) {
		for (int y = 0; y < temp.length; ++y) {				//Gehe solange durch, wie das übergebene Array lang ist
			System.out.print("[");							//Schreibe eine eckige Klammer ;)
			for (int x = 0; x < temp[y].length; ++x) {		//Gehe jetzt solange durch, wie die zweite Dimension lang ist
				System.out.print(temp[y][x]);				//Gebe das jeweils aktuell gewählte Feld aus
			}
			System.out.println("]");						//Schreibe eine weitere eckige Klammer ;)
		}
	}
	public static void print2DArray(int[] temp) {
		for (int y = 0; y < temp.length; y++) {	
			System.out.print(temp[y] + " ;");
		}
	}
	public static void fill(int[] temp, int value) {
		for(int i = 0; i < temp.length; i++) {
			temp[i] = value;
		}
	}
}